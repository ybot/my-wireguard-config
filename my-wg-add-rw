#!/bin/bash
set -Eeuo pipefail
. ${MY_LIB_COMMON:-/lib/mygw/common.sh}

id=$(($1+1))
descr="${2:-}"

: ${MY_WIREGUARD_TMP_PATH:="$(mktemp -d -t my-wireguard-config.XXXX)"}
: ${MY_WIREGUARD_RULE_SEQ:=30000}
: ${MY_WIREGUARD_VPN_RTABLE:=1234}
: ${MY_WIREGUARD_PORT:=50002}
: ${MY_WIREGUARD_INTERFACE:="rw"}
: ${MY_WIREGUARD_CLIENT_FILE:="${MY_WIREGUARD_TMP_PATH}/wg-client.conf"}
: ${MY_WIREGUARD_SERVER_FILE:="${MY_WIREGUARD_TMP_PATH}/wg-srv.conf"}


## server side vars
mykey=$(grep PrivateKey /etc/wireguard/${MY_WIREGUARD_INTERFACE}.conf | sed 's/.* = //')
mypub=$(wg pubkey <<<"${mykey}")
myv4=$(get_ip4_from_nic ${MY_WIREGUARD_INTERFACE})
myv6=$(get_ip6_from_nic ${MY_WIREGUARD_INTERFACE})
myip=$(get_my_primary_ip4)
myend="${myip}:${MY_WIREGUARD_PORT}"

## shared vars
psk=$(wg genpsk)

## client side vars
ckey=$(wg genkey)
cpub=$(wg pubkey <<<"${ckey}")
seq=${MY_WIREGUARD_RULE_SEQ}
cip4=$(my-ipcalc-get-ip-n ${myv4} ${id})
cip6=$(my-ipcalc-get-ip-n ${myv6} ${id})



## server config
cat <<-EOF >${MY_WIREGUARD_SERVER_FILE}
## client ${descr:-undef}
[Peer]
PublicKey = ${cpub}
PresharedKey = ${psk}
AllowedIPs = ${cip4%%/*}, ${cip6%%/*}
PersistentKeepalive = 25
EOF


## client config
cat <<-EOF >${MY_WIREGUARD_CLIENT_FILE}
## ${HOSTNAME}
[Interface]
PrivateKey = ${ckey}
Address = ${cip4}, ${cip6}
DNS = ${MY_NAMESERVERS_IPV6// /,}
#FwMark = off
#Table = ${MY_WIREGUARD_VPN_RTABLE}
#PostUp = ip -4 rule add lookup main suppress_prefixlength 0 pref ${seq}
#PostUp = ip -4 rule add not to ${myip}/32 ipproto udp dport ${MY_WIREGUARD_PORT} lookup ${MY_WIREGUARD_VPN_RTABLE} pref $((seq+10))
#PostUp = ip -6 rule add lookup main suppress_prefixlength 0 pref ${seq}
#PostUp = ip -6 rule add lookup ${MY_WIREGUARD_VPN_RTABLE} pref $((seq+10))
#PreDown = ip -4 rule del lookup main suppress_prefixlength 0 pref ${seq}
#PreDown = ip -4 rule del not to ${myip}/32 ipproto udp dport ${MY_WIREGUARD_PORT} lookup ${MY_WIREGUARD_VPN_RTABLE} pref $((seq+10))
#PreDown = ip -6 rule del lookup main suppress_prefixlength 0 pref ${seq}
#PreDown = ip -6 rule del lookup ${MY_WIREGUARD_VPN_RTABLE} pref $((seq+10))

[Peer]
PresharedKey = ${psk}
PublicKey = ${mypub}
AllowedIPs = ::/0, 0.0.0.0/0
Endpoint = ${myend}
PersistentKeepalive = 25
EOF


## generate qrcode, helpful for mobile clients
cat ${MY_WIREGUARD_CLIENT_FILE} | qrencode -t ansiutf8 >${MY_WIREGUARD_CLIENT_FILE}.qr

cat <<-EOF
	Add server config:
	  - wg addconf ${MY_WIREGUARD_INTERFACE} ${MY_WIREGUARD_SERVER_FILE}

	Add client config:
	  - scan qrcode in ${MY_WIREGUARD_CLIENT_FILE}.qr
	OR
	  - copy: ${MY_WIREGUARD_CLIENT_FILE} to client
	  - and run: wg setconf ${HOSTNAME%%.*} ${MY_WIREGUARD_CLIENT_FILE}

EOF
